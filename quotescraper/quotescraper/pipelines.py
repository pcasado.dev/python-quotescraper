# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

# [IMPORTS]

from quotescraper.models import Quotes, QuoteTags, db_connect, create_tables
from sqlalchemy.orm import sessionmaker


# [SCRAPY PIPELINES]

class QuotescraperPipeline(object):
    """Create pipeline to conduct collected items from quotes spider 
    to a postgresql instance using SQLALchemy
    
    Arguments:
        object {item} -- object representing scraped item (quote)
    """
    
    def __init__(self):
        self.engine = db_connect()
        self.session = sessionmaker(bind=self.engine)

    def process_item(self, item, spider):
        # Create tables and session to handle transactions
        create_tables(self.engine)
        session = self.session()

        try:
            # Create a data model instance based on item quote data
            quote = Quotes()
            quote.quote_author = item['quote_author']
            quote.quote_text = item['quote_text']
            quote.quote_likes = item['quote_likes']
            
            for tag_item in item['quote_tags']:
                tag = QuoteTags()
                tag.tag_name = tag_item
                quote.quote_tags.append(tag)

            # Creates a session to insert collected quote items into the db
            session.add(quote)
            session.commit()

        except Exception as e:
            print(e)
            session.rollback()
            raise

        finally:
            session.close()
