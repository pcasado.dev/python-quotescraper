# ====================================================================================================================================================
#   PROJECT:        python-quotescraper
#   FILE:           quotes_goodreads.py
#   DESCRIPTION:    Basic spider to collect quotes from "Goodreads Quotes" website. Quotes are sent to Scrapy pipeline as individual items
#                   defined on items.py
#   DATE:           June 2019
#   BY:             pcasado
#   REFERENCES:
#       [SCRAPY DOCS - CONSTRUCTING SELECTORS] https://docs.scrapy.org/en/latest/topics/selectors.html#constructing-selectors
# ====================================================================================================================================================

# [IMPORTS]

import scrapy
import re
from random import randint
from time import sleep
from quotescraper.items import QuotescraperItem

# [SPIDER CLASSES]

class GoodReadsSpider(scrapy.Spider):
    """ Scrapy spider to collect data from Goodreads quotes section
    
    Arguments:
        scrapy {Spider Class} -- Scrapy Spider base class
    """

    # Class attributes
    name = 'goodreads_quotes'
    allowed_domains = ['goodreads.com']
    start_urls = ['https://www.goodreads.com/quotes']

    def parse(self, response):
        """ Navigate over Goodreads quotes pages, parse the raw html with xpath,
            and collect the quote attributes as items to be sent to the database
    
        Arguments:
            response -- response from spider requests
        """
        # Create a random pause to space requests made to the website
        sleep(randint(5,10))

        # Create a pipeline item
        quote_item = QuotescraperItem()

        # Grab quote info items from each div
        for quote_div in response.xpath("//div[@class='quote']"):
            quote_item ['quote_text'] = quote_div.xpath(".//div[@class='quoteText']/text()").get().strip()
            quote_item ['quote_author'] = quote_div.xpath(".//div/span[@class='authorOrTitle']/text()").get().strip()
            quote_item ['quote_tags'] = quote_div.xpath(".//div[@class='quoteFooter']/div/a[contains(@href,'tag')]/text()").getall()
            quote_item ['quote_likes'] = re.findall('\d+',quote_div.xpath(".//div[@class='quoteFooter']/div/a[@class='smallText']/text()").get())[0]

            print(
                f""" 
                ============================================================== COLLECTED QUOTE INFO =============================================================
                TEXT:   {quote_item['quote_text']}
                AUTHOR: {quote_item['quote_author']}
                TAGS:   {quote_item['quote_tags']}
                LIKES:  {quote_item['quote_likes']}
                =================================================================================================================================================
            """)

            # Yields quote item to Scrapy pipeline
            yield quote_item

        # Move spider to following pages
        next_page = response.xpath("//a[@class='next_page']/@href").get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

            