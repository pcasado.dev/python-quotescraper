# ====================================================================================================================================================
#   PROJECT:        python-quotescraper
#   FILE:           models.py
#   DESCRIPTION:    Creates the bases to connect to the database and defines the data models
#   DATE:           June 2019
#   BY:             pcasado
#   REFERENCES:
#       [SQLALCHEMY - CONNECTION] https://docs.sqlalchemy.org/en/13/orm/tutorial.html#connecting
#       [SQLALCHEMY RELATIONSHIPS] https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html 
# ====================================================================================================================================================

# [IMPORTS]

from sqlalchemy import create_engine, Column, Integer, String, Text, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from os import getenv


# [DATABASE CONNECTION UTILS]

DeclarativeBase = declarative_base()

def db_connect():
    return create_engine(getenv('CONN_STRING'))

def create_tables(engine):
    DeclarativeBase.metadata.create_all(engine)


# [DATA MODELS]

class QuoteTag_Link(DeclarativeBase):
    __tablename__ = 'quote_tag_link'
    quote_id = Column(Integer, ForeignKey('quotes.id'),primary_key=True)
    tag_id = Column(Integer, ForeignKey('tags.id'),primary_key=True)


class Quotes(DeclarativeBase):
    __tablename__='quotes'
    id = Column(Integer,primary_key=True)
    quote_text = Column(Text) 
    quote_author = Column(String(50))
    quote_likes = Column(Integer)
    quote_tags = relationship('QuoteTags', secondary='quote_tag_link')


class QuoteTags (DeclarativeBase):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    tag_name = Column(String(50))
    quotes = relationship('Quotes', secondary='quote_tag_link')



