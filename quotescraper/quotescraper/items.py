# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class QuotescraperItem(scrapy.Item):
    # define the fields for your item here like:
    quote_text = scrapy.Field()
    quote_author = scrapy.Field() 
    quote_tags = scrapy.Field()
    quote_likes = scrapy.Field()

